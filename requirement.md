# Track Money

## 0.1 Release

The goal of `0.1 Release` is to make a MVP and proof the coverage of user flow. MVP is built using Golang with in-memory data handling (array) and a CLI app.

### Data

#### Account

Account is not used for User Account, but for the Financial Account. i.e. having two different bank accounts will make you have a two different accounts.

```go
type Account struct {
    ID string
    Name string
    Amount int64
}
```

#### Transaction

Save a transaction (in and out) from an account


```go
type Transaction struct {
    ID string
    AccountID string
    Amount int64
    Type string
    Date string
    Note string   
}
```

### CLI

#### Account

1. `track-money-cli account create [account id] [account name] [amount]`, Create new Account
2. `track-money-cli account list` or `track-money-cli account`, List all Account. Flag available `--all` to show all accounts, including the one being archived.
3. `track-money-cli account update [old account id] [new account id] [new name]`,  Update an Account
4. `track-money-cli account archive [account id]`, Archive an Account. Archived account won't be shown unless a flag is used.
5. `track-money-cli account delete [account id]`, Delete an Account.

#### Transaction

1. `track-money-cli transaction [account id] [type] [amount] [date] [note]`, Create new Transaction
2. `track-money-cli transaction` List Transactions with flags `--limit`, `--account`, `--type`
3. `track-money-cli transacton [transaction id] [account id] [type] [amount] [date] [note]`, Update existing Transaction
4. `track-money-cli transaction [transaction id]`, Delete a Transaction
