# Track Money

Track your money wherever you are with a lightweight code by Golang and bunch of integration like a Web Client, a CLI app, Telegram, Discord, REST API and many more.

This source code is designed to be technical and have no reliable documentation for end user at certain phase of development. If you find yourself at a trouble when using this app, please kindly submit an issue through Codeberg repository.

This app is designed to be self-hosted. The cloud service provided in this repository is designed to be for testing or demo that could be down or gone at anytime.

## Todo

- [ ] Define the requirement for `0.1` release
- [ ] Develop the `0.1` release using in-memory data and CLI app as the MVP
- [ ] Develop the `0.2` release by using PostgreSQL and Telegram Integration
